Rails.application.routes.draw do
  root to: redirect('/players')

  get 'players', to: 'site#index'
  get 'players/new', to: 'site#index'
  get 'players/:id', to: 'site#index'
  get 'players/:id/edit', to: 'site#index'

  get 'match_dates', to: 'site#index'
  get 'match_dates/new', to: 'site#index'
  get 'match_dates/:id', to: 'site#index'
  get 'match_dates/:id/edit', to: 'site#index'

  get 'places', to: 'site#index'
  get 'places/new', to: 'site#index'
  get 'places/:id', to: 'site#index'
  get 'places/:id/edit', to: 'site#index'

  get 'rivals', to: 'site#index'
  get 'rivals/new', to: 'site#index'
  get 'rivals/:id', to: 'site#index'
  get 'rivals/:id/edit', to: 'site#index'

  namespace :api do
    namespace :v1 do
      resources :players, only: %i[index show create destroy update]
      resources :match_dates, only: %i[index show create destroy update]
      resources :places, only: %i[index show create destroy update]
      resources :rivals, only: %i[index show create destroy update]
    end
  end
end
