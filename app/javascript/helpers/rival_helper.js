import { error } from './notifications';

export const isEmptyObject = obj => Object.keys(obj).length === 0;

export const handleAjaxError = (err) => {
  error('Algo salio mal...');
  console.warn(err);
};

export const validateRival = (rival) => {
  const errors = {};

  if (rival.name === '') {
    errors.name = 'You must enter an rival type';
  }

  if (rival.goals === '') {
    errors.goals = 'You must enter a valid date';
  }

  return errors;
};
