import { error } from './notifications';

export const isEmptyObject = obj => Object.keys(obj).length === 0;

export const handleAjaxError = (err) => {
  error('Algo salio mal...');
  console.warn(err);
};

export const validateMatchDate = (matchDate) => {
  const errors = {};

  if (matchDate.date === '') {
    errors.date = 'You must enter an matchDate type';
  }

  if (matchDate.goals === '') {
    errors.goals = 'You must enter a valid date';
  }

  if (matchDate.away_goals === '') {
    errors.away_goals = 'You must enter a title';
  }

  return errors;
};

export const formatDate = (d) => {
  const YYYY = d.getFullYear();
  const MM = `0${d.getMonth() + 1}`.slice(-2);
  const DD = `0${d.getDate()}`.slice(-2);

  return `${YYYY}-${MM}-${DD}`;
};
