import { error } from './notifications';

export const isEmptyObject = obj => Object.keys(obj).length === 0;

export const handleAjaxError = (err) => {
  error('Algo salio mal...');
  console.warn(err);
};

export const validatePlayer = (player) => {
  const errors = {};

  if (player.name === '') {
    errors.name = 'You must enter an player type';
  }

  if (player.surname === '') {
    errors.surname = 'You must enter a valid date';
  }

  if (player.condition === '') {
    errors.condition = 'You must enter a title';
  }

  if (player.number === '') {
    errors.number = 'You must enter at least one speaker';
  }

  if (player.goals === '') {
    errors.goals = 'You must enter at least one host';
  }

  return errors;
};
