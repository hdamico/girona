import { error } from './notifications';

export const isEmptyObject = obj => Object.keys(obj).length === 0;

export const handleAjaxError = (err) => {
  error('Algo salio mal...');
  console.warn(err);
};

export const validatePlace = (place) => {
  const errors = {};

  if (place.name === '') {
    errors.name = 'You must enter an place type';
  }

  if (place.address === '') {
    errors.address = 'You must enter a valid date';
  }

  return errors;
};
