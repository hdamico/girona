/* eslint-disable no-undef */
import React from 'react';
import axios from 'axios';
import { Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { success } from '../../helpers/notifications';
import { handleAjaxError } from '../../helpers/match_date_helper';
import '../../../assets/stylesheets/match_dates.scss';
import Header from '../Header';
import PropsRoute from '../PropsRoute';
import MatchDateList from './MatchDateList';
import MatchDate from './MatchDate';
import MatchDateForm from './MatchDateForm';

class MatchDateEditor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      matchDates: null,
      currentUrl: null,
    };
    this.addMatchDate = this.addMatchDate.bind(this);
    this.deleteMatchDate = this.deleteMatchDate.bind(this);
    this.updateMatchDate = this.updateMatchDate.bind(this);
  }

  componentDidMount() {
    axios
      .get('/api/v1/match_dates.json')
      .then(response => this.setState({
        matchDates: response.data,
        currentUrl: window.location.href,
      }))
      .catch(handleAjaxError);
  }

  addMatchDate(newMatchDate) {
    axios
      .post('/api/v1/match_dates.json', newMatchDate)
      .then((response) => {
        success('Fecha agregada!');
        const savedMatchDate = response.data;
        this.setState(prevState => ({
          matchDates: [...prevState.matchDates, savedMatchDate],
        }));
        const { history } = this.props;
        history.push(`/match_dates/${savedMatchDate.id}`);
      })
      .catch(handleAjaxError);
  }

  deleteMatchDate(matchDateId) {
    const sure = window.confirm('Estas seguro?');
    if (sure) {
      axios
        .delete(`/api/v1/match_dates/${matchDateId}.json`)
        .then((response) => {
          if (response.status === 204) {
            success('Fecha eliminada');
            const { history } = this.props;
            history.push('/match_dates');

            const { matchDates } = this.state;
            this.setState({
              matchDates: matchDates.filter(matchDate => matchDate.id !== matchDateId),
            });
          }
        })
        .catch(handleAjaxError);
    }
  }

  updateMatchDate(updatedMatchDate) {
    axios
      .put(`/api/v1/match_dates/${updatedMatchDate.id}.json`, updatedMatchDate)
      .then(() => {
        success('Fecha actualizada');
        const { matchDates } = this.state;
        const idx = matchDates.findIndex(matchDate => matchDate.id === updatedMatchDate.id);
        matchDates[idx] = updatedMatchDate;
        const { history } = this.props;
        history.push(`/match_dates/${updatedMatchDate.id}`);
        this.setState({ matchDates });
      })
      .catch(handleAjaxError);
  }

  render() {
    const { matchDates } = this.state;
    const { currentUrl } = this.state;
    if (matchDates === null) return null;

    const { match } = this.props;
    const matchDateId = match.params.id;
    const matchDate = matchDates.find(e => e.id === Number(matchDateId));

    return (
      <div>
        <Header currentUrl={currentUrl} />
        <div className="grid">
          <MatchDateList matchDates={matchDates} activeId={Number(matchDateId)} />
          <Switch>
            <PropsRoute path="/match_dates/new" component={MatchDateForm} onSubmit={this.addMatchDate} />
            <PropsRoute path="/match_dates/:id/edit" component={MatchDateForm} matchDate={matchDate} onSubmit={this.updateMatchDate} />
            <PropsRoute path="/match_dates/:id" component={MatchDate} matchDate={matchDate} onDelete={this.deleteMatchDate} />
          </Switch>
        </div>
      </div>
    );
  }
}

MatchDateEditor.propTypes = {
  match: PropTypes.shape(),
  history: PropTypes.shape({ push: PropTypes.func }).isRequired,
};

MatchDateEditor.defaultProps = {
  match: undefined,
};

export default MatchDateEditor;
