import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class MatchDateList extends React.Component {
  renderMatchDates() {
    const { activeId, matchDates } = this.props;
    matchDates.sort(
      (a, b) => new Date(b.date) - new Date(a.date),
    );

    return matchDates.map(matchDate => (
      <li key={matchDate.id}>
        <Link to={`/match_dates/${matchDate.id}`} className={activeId === matchDate.id ? 'active' : ''}>
          {matchDate.date}
          {'  ('}
          {matchDate.goals}
          {' - '}
          {matchDate.away_goals}
          {')'}
        </Link>
      </li>
    ));
  }

  render() {
    return (
      <section className="matchDateList">
        <h2>
          Partidos
          <Link to="/match_dates/new">Nuevo Partido</Link>
        </h2>
        <ul>{this.renderMatchDates()}</ul>
      </section>
    );
  }
}

MatchDateList.propTypes = {
  activeId: PropTypes.number,
  matchDates: PropTypes.arrayOf(PropTypes.object),
};

MatchDateList.defaultProps = {
  activeId: undefined,
  matchDates: [],
};

export default MatchDateList;
