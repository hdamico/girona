/* eslint-disable no-new */
import React from 'react';
import PropTypes from 'prop-types';
import Pikaday from 'pikaday';
import 'pikaday/css/pikaday.css';
import { isEmptyObject, validateMatchDate, formatDate } from '../../helpers/match_date_helper';

class MatchDateForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      matchDate: props.matchDate,
      errors: {},
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.dateInput = React.createRef();
  }

  componentDidMount() {
    new Pikaday({
      field: this.dateInput.current,
      onSelect: (date) => {
        const formattedDate = formatDate(date);
        this.dateInput.current.value = formattedDate;
        this.updateMatchDate('date', formattedDate);
      },
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const { matchDate } = this.state;
    const errors = validateMatchDate(matchDate);
    if (!isEmptyObject(errors)) {
      this.setState({ errors });
    } else {
      const { onSubmit } = this.props;
      onSubmit(matchDate);
    }
  }

  handleInputChange(matchDate) {
    const { target } = matchDate;
    const { name } = target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.updateMatchDate(name, value);
  }

  updateMatchDate(key, value) {
    this.setState(prevState => ({
      matchDate: {
        ...prevState.matchDate,
        [key]: value,
      },
    }));
  }

  renderErrors() {
    const { errors } = this.state;

    if (isEmptyObject(errors)) {
      return null;
    }

    return (
      <div className="errors">
        <h3>The following errors prohibited the event from being saved:</h3>
        <ul>
          {Object.values(errors).map(error => (
            <li key={error}>{error}</li>
          ))}
        </ul>
      </div>
    );
  }

  render() {
    const { matchDate } = this.state;

    return (
      <div>
        <h2>Nueva Fecha</h2>
        {this.renderErrors()}
        <form className="matchDateForm" onSubmit={this.handleSubmit}>
          <div>
            <label htmlFor="date">
              <strong>Fecha:</strong>
              <input
                type="text"
                id="date"
                name="date"
                ref={this.dateInput}
                autoComplete="off"
              />
            </label>
          </div>
          <div>
            <label htmlFor="goals">
              <strong>Goles:</strong>
              <input
                type="text"
                id="goals"
                name="goals"
                onChange={this.handleInputChange}
                value={matchDate.goals}
              />
            </label>
          </div>
          <div>
            <label htmlFor="away_goals">
              <strong>Goles rivales:</strong>
              <input
                type="text"
                id="away_goals"
                name="away_goals"
                onChange={this.handleInputChange}
                value={matchDate.away_goals}
              />
            </label>
          </div>
          <div className="form-actions">
            <button type="submit">Guardar</button>
          </div>
        </form>
      </div>
    );
  }
}

MatchDateForm.propTypes = {
  matchDate: PropTypes.shape(),
  onSubmit: PropTypes.func.isRequired,
};

MatchDateForm.defaultProps = {
  matchDate: {
    date: '',
    goals: '',
    away_goals: '',
  },
};

export default MatchDateForm;
