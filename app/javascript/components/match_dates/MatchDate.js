import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const MatchDate = ({ matchDate, onDelete }) => (
  <div className="matchDateContainer">
    <h2>
      {matchDate.date}
      {'  '}
      {matchDate.goals}
      {' - '}
      {matchDate.away_goals}
      {'  '}
      <Link to={`/match_dates/${matchDate.id}/edit`}>Editar</Link>
      <button className="delete" type="button" onClick={() => onDelete(matchDate.id)}>
        Eliminar
      </button>
    </h2>
    <ul>
      <li>
        <strong>Fecha:</strong>
        {' '}
        {matchDate.date}
      </li>
      <li>
        <strong>Goles:</strong>
        {' '}
        {matchDate.goals}
      </li>
      <li>
        <strong>Goles rivales:</strong>
        {' '}
        {matchDate.away_goals}
      </li>
    </ul>
  </div>
);

MatchDate.propTypes = {
  matchDate: PropTypes.shape(),
  onDelete: PropTypes.func.isRequired,
};

MatchDate.defaultProps = {
  matchDate: undefined,
};

export default MatchDate;
