import React from 'react';
import { Route } from 'react-router-dom';
import { Alert } from '../helpers/notifications';
import PlayerEditor from './players/PlayerEditor';
import MatchDateEditor from './match_dates/MatchDateEditor';
import PlaceEditor from './places/PlaceEditor';
import RivalEditor from './rivals/RivalEditor';
import '../../assets/stylesheets/application.css';

const App = () => (
  <div>
    <Route path="/players/:id?" component={PlayerEditor} />
    <Route path="/match_dates/:id?" component={MatchDateEditor} />
    <Route path="/places/:id?" component={PlaceEditor} />
    <Route path="/rivals/:id?" component={RivalEditor} />
    <Alert stack={{ limit: 3 }} />
  </div>
);

export default App;
