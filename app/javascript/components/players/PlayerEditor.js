/* eslint-disable no-undef */
import React from 'react';
import axios from 'axios';
import { Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { success } from '../../helpers/notifications';
import { handleAjaxError } from '../../helpers/player_helper';
import '../../../assets/stylesheets/players.scss';
import Header from '../Header';
import PropsRoute from '../PropsRoute';
import PlayerList from './PlayerList';
import Player from './Player';
import PlayerForm from './PlayerForm';

class PlayerEditor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      players: null,
      currentUrl: null,
    };
    this.addPlayer = this.addPlayer.bind(this);
    this.deletePlayer = this.deletePlayer.bind(this);
    this.updatePlayer = this.updatePlayer.bind(this);
  }

  componentDidMount() {
    axios
      .get('/api/v1/players.json')
      .then(response => this.setState({ players: response.data, currentUrl: window.location.href }))
      .catch(handleAjaxError);
  }

  addPlayer(newPlayer) {
    axios
      .post('/api/v1/players.json', newPlayer)
      .then((response) => {
        success('Jugador agregado!');
        const savedPlayer = response.data;
        this.setState(prevState => ({
          players: [...prevState.players, savedPlayer],
        }));
        const { history } = this.props;
        history.push(`/players/${savedPlayer.id}`);
      })
      .catch(handleAjaxError);
  }

  deletePlayer(playerId) {
    const sure = window.confirm('Estas seguro?');
    if (sure) {
      axios
        .delete(`/api/v1/players/${playerId}.json`)
        .then((response) => {
          if (response.status === 204) {
            success('Jugador eliminado');
            const { history } = this.props;
            history.push('/players');

            const { players } = this.state;
            this.setState({ players: players.filter(player => player.id !== playerId) });
          }
        })
        .catch(handleAjaxError);
    }
  }

  updatePlayer(updatedPlayer) {
    axios
      .put(`/api/v1/players/${updatedPlayer.id}.json`, updatedPlayer)
      .then(() => {
        success('Jugador actualizado');
        const { players } = this.state;
        const idx = players.findIndex(player => player.id === updatedPlayer.id);
        players[idx] = updatedPlayer;
        const { history } = this.props;
        history.push(`/players/${updatedPlayer.id}`);
        this.setState({ players });
      })
      .catch(handleAjaxError);
  }

  render() {
    const { players } = this.state;
    const { currentUrl } = this.state;
    if (players === null) return null;

    const { match } = this.props;
    const playerId = match.params.id;
    const player = players.find(e => e.id === Number(playerId));

    return (
      <div>
        <Header currentUrl={currentUrl} />
        <div className="grid">
          <PlayerList players={players} activeId={Number(playerId)} />
          <Switch>
            <PropsRoute path="/players/new" component={PlayerForm} onSubmit={this.addPlayer} />
            <PropsRoute path="/players/:id/edit" component={PlayerForm} player={player} onSubmit={this.updatePlayer} />
            <PropsRoute path="/players/:id" component={Player} player={player} onDelete={this.deletePlayer} />
          </Switch>
        </div>
      </div>
    );
  }
}

PlayerEditor.propTypes = {
  match: PropTypes.shape(),
  history: PropTypes.shape({ push: PropTypes.func }).isRequired,
};

PlayerEditor.defaultProps = {
  match: undefined,
};

export default PlayerEditor;
