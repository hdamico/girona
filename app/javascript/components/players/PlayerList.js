/* eslint-disable prefer-arrow-callback */
/* eslint-disable func-names */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class PlayerList extends React.Component {
  renderPlayers() {
    const { activeId, players } = this.props;
    players.sort(function (a, b) {
      if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
      if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
      return 0;
    });

    return players.map(player => (
      <li key={player.id}>
        <Link to={`/players/${player.id}`} className={activeId === player.id ? 'active' : ''}>
          {player.name}
          {'  '}
          {player.surname}
        </Link>
      </li>
    ));
  }

  render() {
    return (
      <section className="playerList">
        <h2>
          Jugadores
          <Link to="/players/new">Nuevo Jugador</Link>
        </h2>
        <ul>{this.renderPlayers()}</ul>
      </section>
    );
  }
}

PlayerList.propTypes = {
  activeId: PropTypes.number,
  players: PropTypes.arrayOf(PropTypes.object),
};

PlayerList.defaultProps = {
  activeId: undefined,
  players: [],
};

export default PlayerList;
