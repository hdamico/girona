import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Player = ({ player, onDelete }) => (
  <div className="playerContainer">
    <h2>
      {player.name}
      {'  '}
      {player.surname}
      {'  '}
      <Link to={`/players/${player.id}/edit`}>Editar</Link>
      <button className="delete" type="button" onClick={() => onDelete(player.id)}>
        Eliminar
      </button>
    </h2>
    <ul>
      <li>
        <strong>Nombre:</strong>
        {' '}
        {player.name}
      </li>
      <li>
        <strong>Apellido:</strong>
        {' '}
        {player.surname}
      </li>
      <li>
        <strong>Pie:</strong>
        {' '}
        {player.condition}
      </li>
      <li>
        <strong>Numero:</strong>
        {' '}
        {player.number}
      </li>
      <li>
        <strong>Goles:</strong>
        {' '}
        {player.goals}
      </li>
    </ul>
  </div>
);

Player.propTypes = {
  player: PropTypes.shape(),
  onDelete: PropTypes.func.isRequired,
};

Player.defaultProps = {
  player: undefined,
};

export default Player;
