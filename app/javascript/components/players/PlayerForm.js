import React from 'react';
import PropTypes from 'prop-types';
import { isEmptyObject, validatePlayer } from '../../helpers/player_helper';

class PlayerForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      player: props.player,
      errors: {},
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { player } = this.state;
    const errors = validatePlayer(player);
    if (!isEmptyObject(errors)) {
      this.setState({ errors });
    } else {
      const { onSubmit } = this.props;
      onSubmit(player);
    }
  }

  handleInputChange(player) {
    const { target } = player;
    const { name } = target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.updatePlayer(name, value);

    this.setState(prevState => ({
      player: {
        ...prevState.player,
        [name]: value,
      },
    }));
  }

  updatePlayer(key, value) {
    this.setState(prevState => ({
      player: {
        ...prevState.player,
        [key]: value,
      },
    }));
  }

  renderErrors() {
    const { errors } = this.state;

    if (isEmptyObject(errors)) {
      return null;
    }

    return (
      <div className="errors">
        <h3>The following errors prohibited the event from being saved:</h3>
        <ul>
          {Object.values(errors).map(error => (
            <li key={error}>{error}</li>
          ))}
        </ul>
      </div>
    );
  }

  render() {
    const { player } = this.state;

    return (
      <div>
        <h2>Nuevo Jugador</h2>
        {this.renderErrors()}
        <form className="playerForm" onSubmit={this.handleSubmit}>
          <div>
            <label htmlFor="name">
              <strong>Nombre:</strong>
              <input
                type="text"
                id="name"
                name="name"
                onChange={this.handleInputChange}
                value={player.name}
              />
            </label>
          </div>
          <div>
            <label htmlFor="surname">
              <strong>Apellido:</strong>
              <input
                type="text"
                id="surname"
                name="surname"
                onChange={this.handleInputChange}
                value={player.surname}
              />
            </label>
          </div>
          <div>
            <label htmlFor="condition">
              <strong>Pie:</strong>
              <input
                type="text"
                id="condition"
                name="condition"
                onChange={this.handleInputChange}
                value={player.condition}
              />
            </label>
          </div>
          <div>
            <label htmlFor="number">
              <strong>Numero:</strong>
              <input
                type="text"
                id="number"
                name="number"
                onChange={this.handleInputChange}
                value={player.number}
              />
            </label>
          </div>
          <div>
            <label htmlFor="goals">
              <strong>Goles:</strong>
              <input type="text" id="goals" name="goals" onChange={this.handleInputChange} value={player.goals} />
            </label>
          </div>
          <div className="form-actions">
            <button type="submit">Guardar</button>
          </div>
        </form>
      </div>
    );
  }
}

PlayerForm.propTypes = {
  player: PropTypes.shape(),
  onSubmit: PropTypes.func.isRequired,
};

PlayerForm.defaultProps = {
  player: {
    name: '',
    surname: '',
    condition: '',
    number: '',
    goals: '',
  },
};

export default PlayerForm;
