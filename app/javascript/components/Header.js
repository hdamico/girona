import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Header = ({ currentUrl }) => (
  <header>
    <h1>Girona</h1>
    <Link to="/players">
      <h1 className={currentUrl.includes('players') ? 'active' : ''}> Jugadores </h1>
    </Link>
    <Link to="/match_dates">
      <h1 className={currentUrl.includes('match_dates') ? 'active' : ''}> Partidos </h1>
    </Link>
    <Link to="/places">
      <h1 className={currentUrl.includes('places') ? 'active' : ''}> Sedes </h1>
    </Link>
    <Link to="/rivals">
      <h1 className={currentUrl.includes('rivals') ? 'active' : ''}> Rivales </h1>
    </Link>
  </header>
);

Header.propTypes = {
  currentUrl: PropTypes.string,
};

Header.defaultProps = {
  currentUrl: undefined,
};

export default Header;
