import React from 'react';
import PropTypes from 'prop-types';
import { isEmptyObject, validatePlace } from '../../helpers/place_helper';

class PlaceForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      place: props.place,
      errors: {},
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { place } = this.state;
    const errors = validatePlace(place);
    if (!isEmptyObject(errors)) {
      this.setState({ errors });
    } else {
      const { onSubmit } = this.props;
      onSubmit(place);
    }
  }

  handleInputChange(place) {
    const { target } = place;
    const { name } = target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.updatePlace(name, value);

    this.setState(prevState => ({
      place: {
        ...prevState.place,
        [name]: value,
      },
    }));
  }

  updatePlace(key, value) {
    this.setState(prevState => ({
      place: {
        ...prevState.place,
        [key]: value,
      },
    }));
  }

  renderErrors() {
    const { errors } = this.state;

    if (isEmptyObject(errors)) {
      return null;
    }

    return (
      <div className="errors">
        <h3>The following errors prohibited the event from being saved:</h3>
        <ul>
          {Object.values(errors).map(error => (
            <li key={error}>{error}</li>
          ))}
        </ul>
      </div>
    );
  }

  render() {
    const { place } = this.state;

    return (
      <div>
        <h2>Nueva Sede</h2>
        {this.renderErrors()}
        <form className="placeForm" onSubmit={this.handleSubmit}>
          <div>
            <label htmlFor="name">
              <strong>Nombre:</strong>
              <input
                type="text"
                id="name"
                name="name"
                onChange={this.handleInputChange}
                value={place.name}
              />
            </label>
          </div>
          <div>
            <label htmlFor="address">
              <strong>Direccion:</strong>
              <input
                type="text"
                id="address"
                name="address"
                onChange={this.handleInputChange}
                value={place.address}
              />
            </label>
          </div>
          <div className="form-actions">
            <button type="submit">Guardar</button>
          </div>
        </form>
      </div>
    );
  }
}

PlaceForm.propTypes = {
  place: PropTypes.shape(),
  onSubmit: PropTypes.func.isRequired,
};

PlaceForm.defaultProps = {
  place: {
    name: '',
    surname: '',
    condition: '',
    number: '',
    goals: '',
  },
};

export default PlaceForm;
