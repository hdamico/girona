/* eslint-disable no-undef */
import React from 'react';
import axios from 'axios';
import { Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { success } from '../../helpers/notifications';
import { handleAjaxError } from '../../helpers/place_helper';
import '../../../assets/stylesheets/places.scss';
import Header from '../Header';
import PropsRoute from '../PropsRoute';
import PlaceList from './PlaceList';
import Place from './Place';
import PlaceForm from './PlaceForm';

class PlaceEditor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      places: null,
      currentUrl: null,
    };
    this.addPlace = this.addPlace.bind(this);
    this.deletePlace = this.deletePlace.bind(this);
    this.updatePlace = this.updatePlace.bind(this);
  }

  componentDidMount() {
    axios
      .get('/api/v1/places.json')
      .then(response => this.setState({ places: response.data, currentUrl: window.location.href }))
      .catch(handleAjaxError);
  }

  addPlace(newPlace) {
    axios
      .post('/api/v1/places.json', newPlace)
      .then((response) => {
        success('Jugador agregado!');
        const savedPlace = response.data;
        this.setState(prevState => ({
          places: [...prevState.places, savedPlace],
        }));
        const { history } = this.props;
        history.push(`/places/${savedPlace.id}`);
      })
      .catch(handleAjaxError);
  }

  deletePlace(placeId) {
    const sure = window.confirm('Estas seguro?');
    if (sure) {
      axios
        .delete(`/api/v1/places/${placeId}.json`)
        .then((response) => {
          if (response.status === 204) {
            success('Jugador eliminado');
            const { history } = this.props;
            history.push('/places');

            const { places } = this.state;
            this.setState({ places: places.filter(place => place.id !== placeId) });
          }
        })
        .catch(handleAjaxError);
    }
  }

  updatePlace(updatedPlace) {
    axios
      .put(`/api/v1/places/${updatedPlace.id}.json`, updatedPlace)
      .then(() => {
        success('Jugador actualizado');
        const { places } = this.state;
        const idx = places.findIndex(place => place.id === updatedPlace.id);
        places[idx] = updatedPlace;
        const { history } = this.props;
        history.push(`/places/${updatedPlace.id}`);
        this.setState({ places });
      })
      .catch(handleAjaxError);
  }

  render() {
    const { places } = this.state;
    const { currentUrl } = this.state;
    if (places === null) return null;

    const { match } = this.props;
    const placeId = match.params.id;
    const place = places.find(e => e.id === Number(placeId));

    return (
      <div>
        <Header currentUrl={currentUrl} />
        <div className="grid">
          <PlaceList places={places} activeId={Number(placeId)} />
          <Switch>
            <PropsRoute path="/places/new" component={PlaceForm} onSubmit={this.addPlace} />
            <PropsRoute path="/places/:id/edit" component={PlaceForm} place={place} onSubmit={this.updatePlace} />
            <PropsRoute path="/places/:id" component={Place} place={place} onDelete={this.deletePlace} />
          </Switch>
        </div>
      </div>
    );
  }
}

PlaceEditor.propTypes = {
  match: PropTypes.shape(),
  history: PropTypes.shape({ push: PropTypes.func }).isRequired,
};

PlaceEditor.defaultProps = {
  match: undefined,
};

export default PlaceEditor;
