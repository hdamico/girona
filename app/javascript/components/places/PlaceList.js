import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class PlaceList extends React.Component {
  renderPlaces() {
    const { activeId, places } = this.props;
    places.sort(
      (a, b) => new Date(b.place_date) - new Date(a.place_date),
    );

    return places.map(place => (
      <li key={place.id}>
        <Link to={`/places/${place.id}`} className={activeId === place.id ? 'active' : ''}>
          {place.name}
        </Link>
      </li>
    ));
  }

  render() {
    return (
      <section className="placeList">
        <h2>
          Sedes
          <Link to="/places/new">Nueva Sede</Link>
        </h2>
        <ul>{this.renderPlaces()}</ul>
      </section>
    );
  }
}

PlaceList.propTypes = {
  activeId: PropTypes.number,
  places: PropTypes.arrayOf(PropTypes.object),
};

PlaceList.defaultProps = {
  activeId: undefined,
  places: [],
};

export default PlaceList;
