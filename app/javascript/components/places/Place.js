import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Place = ({ place, onDelete }) => (
  <div className="placeContainer">
    <h2>
      {place.name}
      {'  '}
      <Link to={`/places/${place.id}/edit`}>Editar</Link>
      <button className="delete" type="button" onClick={() => onDelete(place.id)}>
        Eliminar
      </button>
    </h2>
    <ul>
      <li>
        <strong>Nombre:</strong>
        {' '}
        {place.name}
      </li>
      <li>
        <strong>Direccion:</strong>
        {' '}
        {place.address}
      </li>
    </ul>
  </div>
);

Place.propTypes = {
  place: PropTypes.shape(),
  onDelete: PropTypes.func.isRequired,
};

Place.defaultProps = {
  place: undefined,
};

export default Place;
