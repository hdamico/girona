import React from 'react';
import PropTypes from 'prop-types';
import { isEmptyObject, validateRival } from '../../helpers/rival_helper';

class RivalForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      player: props.player,
      errors: {},
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { player } = this.state;
    const errors = validateRival(player);
    if (!isEmptyObject(errors)) {
      this.setState({ errors });
    } else {
      const { onSubmit } = this.props;
      onSubmit(player);
    }
  }

  handleInputChange(player) {
    const { target } = player;
    const { name } = target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    this.updateRival(name, value);

    this.setState(prevState => ({
      player: {
        ...prevState.player,
        [name]: value,
      },
    }));
  }

  updateRival(key, value) {
    this.setState(prevState => ({
      player: {
        ...prevState.player,
        [key]: value,
      },
    }));
  }

  renderErrors() {
    const { errors } = this.state;

    if (isEmptyObject(errors)) {
      return null;
    }

    return (
      <div className="errors">
        <h3>The following errors prohibited the event from being saved:</h3>
        <ul>
          {Object.values(errors).map(error => (
            <li key={error}>{error}</li>
          ))}
        </ul>
      </div>
    );
  }

  render() {
    const { player } = this.state;

    return (
      <div>
        <h2>Nuevo Rival</h2>
        {this.renderErrors()}
        <form className="rivalForm" onSubmit={this.handleSubmit}>
          <div>
            <label htmlFor="name">
              <strong>Nombre:</strong>
              <input
                type="text"
                id="name"
                name="name"
                onChange={this.handleInputChange}
                value={player.name}
              />
            </label>
          </div>
          <div>
            <label htmlFor="goals">
              <strong>Goles:</strong>
              <input
                type="text"
                id="goals"
                name="goals"
                onChange={this.handleInputChange}
                value={player.goals}
              />
            </label>
          </div>
          <div className="form-actions">
            <button type="submit">Guardar</button>
          </div>
        </form>
      </div>
    );
  }
}

RivalForm.propTypes = {
  player: PropTypes.shape(),
  onSubmit: PropTypes.func.isRequired,
};

RivalForm.defaultProps = {
  player: {
    name: '',
    goals: '',
  },
};

export default RivalForm;
