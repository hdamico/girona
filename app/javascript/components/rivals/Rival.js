import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Rival = ({ rival, onDelete }) => (
  <div className="rivalContainer">
    <h2>
      {rival.name}
      <Link to={`/rivals/${rival.id}/edit`}>Editar</Link>
      <button className="delete" type="button" onClick={() => onDelete(rival.id)}>
        Eliminar
      </button>
    </h2>
    <ul>
      <li>
        <strong>Nombre:</strong>
        {' '}
        {rival.name}
      </li>
      <li>
        <strong>Goles:</strong>
        {' '}
        {rival.goals}
      </li>
    </ul>
  </div>
);

Rival.propTypes = {
  rival: PropTypes.shape(),
  onDelete: PropTypes.func.isRequired,
};

Rival.defaultProps = {
  rival: undefined,
};

export default Rival;
