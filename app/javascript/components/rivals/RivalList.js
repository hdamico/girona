import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class RivalList extends React.Component {
  renderRivals() {
    const { activeId, rivals } = this.props;
    rivals.sort(
      (a, b) => new Date(b.rival_date) - new Date(a.rival_date),
    );

    return rivals.map(rival => (
      <li key={rival.id}>
        <Link to={`/rivals/${rival.id}`} className={activeId === rival.id ? 'active' : ''}>
          {rival.name}
        </Link>
      </li>
    ));
  }

  render() {
    return (
      <section className="rivalList">
        <h2>
          Rivales
          <Link to="/rivals/new">Nuevo Rival</Link>
        </h2>
        <ul>{this.renderRivals()}</ul>
      </section>
    );
  }
}

RivalList.propTypes = {
  activeId: PropTypes.number,
  rivals: PropTypes.arrayOf(PropTypes.object),
};

RivalList.defaultProps = {
  activeId: undefined,
  rivals: [],
};

export default RivalList;
