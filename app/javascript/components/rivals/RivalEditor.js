/* eslint-disable no-undef */
import React from 'react';
import axios from 'axios';
import { Switch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { success } from '../../helpers/notifications';
import { handleAjaxError } from '../../helpers/rival_helper';
import '../../../assets/stylesheets/rivals.scss';
import Header from '../Header';
import PropsRoute from '../PropsRoute';
import RivalList from './RivalList';
import Rival from './Rival';
import RivalForm from './RivalForm';

class RivalEditor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rivals: null,
      currentUrl: null,
    };
    this.addRival = this.addRival.bind(this);
    this.deleteRival = this.deleteRival.bind(this);
    this.updateRival = this.updateRival.bind(this);
  }

  componentDidMount() {
    axios
      .get('/api/v1/rivals.json')
      .then(response => this.setState({ rivals: response.data, currentUrl: window.location.href }))
      .catch(handleAjaxError);
  }

  addRival(newRival) {
    axios
      .post('/api/v1/rivals.json', newRival)
      .then((response) => {
        success('Jugador agregado!');
        const savedRival = response.data;
        this.setState(prevState => ({
          rivals: [...prevState.rivals, savedRival],
        }));
        const { history } = this.props;
        history.push(`/rivals/${savedRival.id}`);
      })
      .catch(handleAjaxError);
  }

  deleteRival(rivalId) {
    const sure = window.confirm('Estas seguro?');
    if (sure) {
      axios
        .delete(`/api/v1/rivals/${rivalId}.json`)
        .then((response) => {
          if (response.status === 204) {
            success('Jugador eliminado');
            const { history } = this.props;
            history.push('/rivals');

            const { rivals } = this.state;
            this.setState({ rivals: rivals.filter(rival => rival.id !== rivalId) });
          }
        })
        .catch(handleAjaxError);
    }
  }

  updateRival(updatedRival) {
    axios
      .put(`/api/v1/rivals/${updatedRival.id}.json`, updatedRival)
      .then(() => {
        success('Jugador actualizado');
        const { rivals } = this.state;
        const idx = rivals.findIndex(rival => rival.id === updatedRival.id);
        rivals[idx] = updatedRival;
        const { history } = this.props;
        history.push(`/rivals/${updatedRival.id}`);
        this.setState({ rivals });
      })
      .catch(handleAjaxError);
  }

  render() {
    const { rivals } = this.state;
    const { currentUrl } = this.state;
    if (rivals === null) return null;

    const { match } = this.props;
    const rivalId = match.params.id;
    const rival = rivals.find(e => e.id === Number(rivalId));

    return (
      <div>
        <Header currentUrl={currentUrl} />
        <div className="grid">
          <RivalList rivals={rivals} activeId={Number(rivalId)} />
          <Switch>
            <PropsRoute path="/rivals/new" component={RivalForm} onSubmit={this.addRival} />
            <PropsRoute path="/rivals/:id/edit" component={RivalForm} rival={rival} onSubmit={this.updateRival} />
            <PropsRoute path="/rivals/:id" component={Rival} rival={rival} onDelete={this.deleteRival} />
          </Switch>
        </div>
      </div>
    );
  }
}

RivalEditor.propTypes = {
  match: PropTypes.shape(),
  history: PropTypes.shape({ push: PropTypes.func }).isRequired,
};

RivalEditor.defaultProps = {
  match: undefined,
};

export default RivalEditor;
