class MatchDate < ApplicationRecord
  has_many :player_match_date, dependent: :destroy
  has_many :players, through: :player_match_dates
  has_one :place
  has_one :rival
end
