class Player < ApplicationRecord
  has_many :player_match_dates, dependent: :destroy
  has_many :dates, through: :player_match_dates
end
