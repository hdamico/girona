class Api::V1::RivalsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_rival, only: %i[show update destroy]
  respond_to :json

  def index
    respond_with Rival.order(id: :DESC)
  end

  def show
    respond_with Rival.find(params[:id])
  end

  def create
    @rival = Rival.create(rival_params)
    respond_with Rival, json: @rival
  end

  def update
    @rival.update(rival_params)
    respond_with Rival, json: @rival
  end

  def destroy
    respond_with @rival.destroy
  end

  private

  def set_rival
    @rival = Rival.find(params[:id])
  end

  def rival_params
    params.require(:rival).permit(
      :rival_id, :name, :goals
    )
  end
end
