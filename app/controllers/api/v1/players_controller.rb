class Api::V1::PlayersController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_player, only: %i[show update destroy]
  respond_to :json

  def index
    respond_with Player.order(id: :DESC)
  end

  def show
    respond_with Player.find(params[:id])
  end

  def create
    @player = Player.create(player_params)
    respond_with Player, json: @player
  end

  def update
    @player.update(player_params)
    respond_with Player, json: @player
  end

  def destroy
    respond_with @player.destroy
  end

  private

  def set_player
    @player = Player.find(params[:id])
  end

  def player_params
    params.require(:player).permit(
      :player_id, :name, :surname, :number, :condition, :fit,
      :goals, :assists
    )
  end
end
