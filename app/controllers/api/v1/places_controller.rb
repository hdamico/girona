class Api::V1::PlacesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_place, only: %i[show update destroy]
  respond_to :json

  def index
    respond_with Place.order(id: :DESC)
  end

  def show
    respond_with Place.find(params[:id])
  end

  def create
    @place = Place.create(place_params)
    respond_with Place, json: @place
  end

  def update
    @place.update(place_params)
    respond_with Place, json: @place
  end

  def destroy
    respond_with @place.destroy
  end

  private

  def set_place
    @place = Place.find(params[:id])
  end

  def place_params
    params.require(:place).permit(
      :place_id, :name, :address
    )
  end
end
