class Api::V1::MatchDatesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_match_date, only: %i[show update destroy]
  respond_to :json

  def index
    respond_with MatchDate.order(id: :DESC)
  end

  def show
    respond_with MatchDate.find(params[:id])
  end

  def create
    @match_date = MatchDate.create(match_date_params)
    respond_with MatchDate, json: @match_date
  end

  def update
    @match_date.update(match_date_params)
    respond_with MatchDate, json: @match_date
  end

  def destroy
    respond_with @match_date.destroy
  end

  private

  def set_match_date
    @match_date = MatchDate.find(params[:id])
  end

  def match_date_params
    params.require(:match_date).permit(
      :match_date_id, :date, :goals, :away_goals
    )
  end
end
