# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_22_202151) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "match_dates", force: :cascade do |t|
    t.date "date"
    t.integer "goals", default: 0
    t.integer "away_goals", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "places", force: :cascade do |t|
    t.bigint "match_date_id"
    t.string "name", null: false
    t.string "address", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["match_date_id"], name: "index_places_on_match_date_id"
  end

  create_table "player_match_dates", force: :cascade do |t|
    t.bigint "player_id"
    t.bigint "match_date_id"
    t.boolean "best"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["match_date_id"], name: "index_player_match_dates_on_match_date_id"
    t.index ["player_id"], name: "index_player_match_dates_on_player_id"
  end

  create_table "players", force: :cascade do |t|
    t.string "name", null: false
    t.string "surname", null: false
    t.integer "number", null: false
    t.string "condition"
    t.boolean "fit"
    t.integer "goals", default: 0
    t.integer "assists", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rivals", force: :cascade do |t|
    t.bigint "match_date_id"
    t.string "name", null: false
    t.integer "goals", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["match_date_id"], name: "index_rivals_on_match_date_id"
  end

  add_foreign_key "places", "match_dates"
  add_foreign_key "player_match_dates", "match_dates"
  add_foreign_key "player_match_dates", "players"
  add_foreign_key "rivals", "match_dates"
end
