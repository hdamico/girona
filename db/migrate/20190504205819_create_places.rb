class CreatePlaces < ActiveRecord::Migration[5.2]
  def change
    create_table :places do |t|
      t.references :match_date, foreign_key: true
      t.string :name, null: false
      t.string :address, null: false

      t.timestamps null: false
    end
  end
end
