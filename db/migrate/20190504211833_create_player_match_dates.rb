class CreatePlayerMatchDates < ActiveRecord::Migration[5.2]
  def change
    create_table :player_match_dates do |t|
      t.references :player, foreign_key: true
      t.references :match_date, foreign_key: true
      t.boolean :best

      t.timestamps null: false
    end
  end
end
