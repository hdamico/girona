class CreateRivals < ActiveRecord::Migration[5.2]
  def change
    create_table :rivals do |t|
      t.references :match_date, foreign_key: true
      t.string :name, null: false
      t.integer :goals, default: 0

      t.timestamps null: false
    end
  end
end
