class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.string :name, null: false
      t.string :surname, null: false
      t.integer :number, null: false
      t.string :condition
      t.boolean :fit
      t.integer :goals, default: 0
      t.integer :assists, default: 0

      t.timestamps null: false
    end
  end
end
