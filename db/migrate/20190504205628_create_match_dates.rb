class CreateMatchDates < ActiveRecord::Migration[5.2]
  def change
    create_table :match_dates do |t|
      t.date :match_date
      t.integer :goals, default: 0
      t.integer :away_goals, default: 0

      t.timestamps null: false
    end
  end
end
