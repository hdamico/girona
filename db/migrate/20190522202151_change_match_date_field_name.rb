class ChangeMatchDateFieldName < ActiveRecord::Migration[5.2]
  def change
    rename_column :match_dates, :match_date, :date
  end
end
